# Hey Cinema

A simple app that uses omdb api to list movies and their ratings, with a search functionality.

The App is published on **[https://heycinema.netlify.app/](https://heycinema.netlify.app/)**

![The app](./docs/screenshot.png)

## Run locally

1. Clone the repo
2. `cd hey-movies`
3. `yarn install`
4. `yarn start`
5. `yarn test` to run the tests

## Unit Tests

The tests coverage stands at 99% for statements (96% for branches). While test coverage should not be the ultimate goal, keeping it around ~90s is something to aim for. More importantly is the quality of tests, I try to use the mantra of not mocking too much, hence the use of `nock` for example in the tests for the top level `App.jsx` to exercise as many layers of the app as possible. This follows the mantra described here: [Write tests. Mostly Integration](https://kentcdodds.com/blog/write-tests/)

![Test coverage](./docs/coverage.png)
