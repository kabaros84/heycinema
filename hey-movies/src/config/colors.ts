export const PrimaryColor = '#7FAAE4';
export const SecondaryColor = '#834DB7';
export const TertiaryColor = '#34283F';
export const DimmedColor = '#aeaeb1';
export const BorderColor = '#dcdcdc';

