import styled, { createGlobalStyle } from "styled-components";
import { PrimaryColor, DimmedColor, BorderColor } from "../config/colors";
import { normalize } from 'styled-normalize';

export const DefaultShadow = `box-shadow: 0px 2px 9px 0px ${BorderColor}`;

export const GlobalStyle = createGlobalStyle`
    ${normalize};

    body {
        font-family: 'Helvetica Neue';
        font-size: 16px;
        padding: 10px;
    }
`;

type HeadingProps = { condensed?: boolean };

export const Heading = styled.h1`
    color: ${(props) => props.color || PrimaryColor};
    font-size: 1.125rem;
    font-weight: bold;
    line-height: ${(props: HeadingProps) => props.condensed ? '5px' : 'inherit'};
`;

export const Subtitle = styled.p`
    color: ${(props) => props.color || DimmedColor};
`;
