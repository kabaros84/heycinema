import Loader from '../Loader';
import { render } from '../../utils/test-utils';

describe('Loader', () => {
    /** I wouldn't test styled components directly unless it's a team convention
     * they are an implementation detail and can be tested through the component that they use them
     * whether we use styled component or not should be transparent
     */
    it('should render the loader', () => {
        const { container } = render(<Loader />);
        expect(container.firstChild).toMatchSnapshot();
    });
});
