import styled from "styled-components";
import { BorderColor } from "../config/colors";
import { DefaultShadow } from "./common";

export const Input = styled.input`
flex: 1 0;
/* padding: 10px; */
border: 0;
&:focus {
    outline: none;
}
`;

export const InputContainer = styled.div`
padding: 10px;
margin-bottom: 20px;
margin-left: auto;
margin-right: auto;
max-width: 400px;
display: flex;
flex-wrap: wrap;
flex: 1 0;
align-items: center;
border-radius: 10px;
border: 1px solid ${BorderColor};
${DefaultShadow};
`;

export const Icon = styled.div`
flex: 0 0;
`;