import styled, { keyframes } from "styled-components";
import { SecondaryColor, PrimaryColor } from "../config/colors";

const rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;

type SpinnerProps = {
  small?: boolean;
}

const Spinner = styled.div`
  animation: ${rotate360} 1s linear infinite;
  transform: translateZ(0);
  
  border-top: 2px solid ${PrimaryColor};
  border-right: 2px solid ${PrimaryColor};
  border-bottom: 2px solid ${PrimaryColor};
  border-left: 4px solid ${SecondaryColor};
  background: transparent;
  width: ${(props: SpinnerProps) => props.small ? '16px' : '24px'};
  height: ${(props: SpinnerProps) => props.small ? '16px' : '24px'};
  border-radius: 50%;
`;

export default Spinner;