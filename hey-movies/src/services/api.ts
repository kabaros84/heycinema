const API_KEY = process.env.REACT_APP_API_KEY || '11f26f94';

export type Movie = {
    Title: string;
    Year: number;
    imdbID: string;
    Type: string;
    Poster: string;
}
export type MovieApiResponse = {
    Search: Movie[]
}
export const searchApi: (searchValue: string) => Promise<MovieApiResponse> = (searchValue) => {
    return fetch(`https://www.omdbapi.com/?s=${searchValue}&apikey=${API_KEY}`).then(res => {
        if (res.status === 200) {
            return res.json()
        } else throw new Error("Something went wrong");
    })
}