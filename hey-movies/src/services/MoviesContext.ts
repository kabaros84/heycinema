import React from 'react';
import { Movie } from './api';

export type SearchResultType = {
    movies: Movie[];
    searchString: string;
}

export type MoviesContextType = {
    searchResult: SearchResultType | undefined;
    setLoading: React.Dispatch<React.SetStateAction<boolean>>;
    loading: boolean;
    errorMessage: string | undefined;
}

const MoviesContext = React.createContext<MoviesContextType | undefined>(undefined);

export const MoviesContextProvider = MoviesContext.Provider;


export const useMoviesContext = () => {
    const context = React.useContext(MoviesContext);
    if (context) return context;
    else throw new Error('no context set');
}
