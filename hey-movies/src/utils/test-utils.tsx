import React from 'react'
import { render } from '@testing-library/react'
import { MoviesContextProvider, MoviesContextType } from '../services/MoviesContext';

const AllTheProviders = ({ children }: { children: React.ReactElement }) => {
    return (
        <MoviesContextProvider value={undefined}>
            { children}
        </MoviesContextProvider>
    )
}


export const renderWithContext = (ui: React.ReactElement, { providerProps, ...renderOptions }: { providerProps: Partial<MoviesContextType> }) => {
    return render(
        <MoviesContextProvider value={providerProps as MoviesContextType}>{ui}</MoviesContextProvider>,
        renderOptions
    )
}


const customRender = (ui: React.ReactElement, options?: any) =>
    render(ui, { wrapper: AllTheProviders, ...options })

// re-export everything
export * from '@testing-library/react'

// override render method
export { customRender as render }