import { FunctionComponent } from "react";
import styled from "styled-components";
import { Movie } from "../services/api";
import { SecondaryColor } from "../config/colors";
import { DefaultShadow, Heading, Subtitle } from "../styledComponents/common";
import { useMoviesContext } from "../services/MoviesContext";

type CardProps = {
    $loading?: boolean;
}

const Card = styled.div`
    ${DefaultShadow};
    display: inline-flex;
    flex-direction: row;
    margin: 10px;
    height: 200px;
    border-radius: 5px;
    background-color: white;
    overflow: hidden;
    width: 100%;

    opacity: ${(props: CardProps) => props.$loading ? 0.2 : 1};

    @media(min-width: 768px) {
        height: 250px;
        width: 30%;
    }
`;

const MovieImageContainer = styled.div`
    flex: 1;
    transform-origin: 100% 0;
    transform: skew(-10deg);
`;

const MovieImage = styled.img`
    width: 150px;
    height: 200px;
    @media(min-width: 768px) {
        width: 200px;
        height: 300px;
    }
`;

const MovieInfoContainer = styled.div`
    flex: 2;
    flex-grow: auto;
    padding: 0px 10px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
`;

const Link = styled.a`
    color: ${SecondaryColor};
    font-weight: 500;
    margin-bottom: 10px;
    text-decoration: none;
`;

export interface MovieCardProps {
    movie: Movie;
}

const MovieCard: FunctionComponent<MovieCardProps> = ({ movie }) => {
    const hasPhoto = movie.Poster && movie.Poster !== 'N/A';
    const { loading = false } = useMoviesContext();

    return (<Card data-testid="movie-card" $loading={loading}>
        <MovieImageContainer>
            {hasPhoto && <MovieImage alt={movie.Title} src={movie.Poster} />}
            {!hasPhoto && <MovieImage alt="No poster" src={"/images/notApplicable.png"} />}
        </MovieImageContainer>
        <MovieInfoContainer>
            <div>
                <Heading>{movie.Title}</Heading>
                <Subtitle>{movie.Year}</Subtitle>
            </div>
            <Link target="_blank" href={`https://imdb.com/title/${movie.imdbID}`}>Check on Imdb</Link>
        </MovieInfoContainer>
    </Card>);
}

export default MovieCard;