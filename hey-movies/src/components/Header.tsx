import { PrimaryColor, SecondaryColor } from "../config/colors";
import { DefaultShadow, Heading } from "../styledComponents/common";
import styled from "styled-components";


const Nav = styled.nav`
    /* ${DefaultShadow}; */
    min-height: 50px;
    padding: 10px 10px;
    /* margin-bottom: 20px; */
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
`;

const Header: React.FunctionComponent = () => {
    return (<Nav>
        <Heading condensed color={PrimaryColor}>hey</Heading>
        <Heading condensed color={SecondaryColor}>cinema</Heading>
    </Nav>);
}

export default Header;