import App from '../App';
import nock from 'nock';
import { render, screen, waitFor } from '../../utils/test-utils';
import { MovieApiResponse } from '../../services/api';
import userEvent from '@testing-library/user-event';

describe.only('App', () => {
  const response: MovieApiResponse = {
    Search: [{
      Title: 'God Father',
      Year: 1990,
      imdbID: 'I-121',
      Type: 'movie',
      Poster: 'http://photo.png'
    }]
  };

  beforeEach(() => {

  });

  afterEach(() => {
    nock.cleanAll();
  });

  describe('when results returned', () => {
    beforeEach(() => {
      nock('https://www.omdbapi.com')
        .defaultReplyHeaders({
          'access-control-allow-origin': '*'
        })
        .get(/s=ala&apikey=.+$/).reply(200, response);
      render(<App />);
    });
    it('should call the API and render the result', async () => {
      jest.useFakeTimers('modern'); // for debouncing
      userEvent.type(screen.getByTestId('search'), 'ala');
      jest.runAllTimers();
      await waitFor(() => screen.getByText('God Father'));
      expect(screen.queryAllByTestId('movie-card').length).toEqual(1);
    });
  });
  describe('when empty results', () => {
    beforeEach(() => {
      nock('https://www.omdbapi.com')
        .defaultReplyHeaders({
          'access-control-allow-origin': '*'
        })
        .get(/s=bbc&apikey=.+$/).reply(200, { Search: [] });
      render(<App />);
    });
    it('should show a message for "No results"', async () => {
      jest.useFakeTimers('modern'); // for debouncing
      userEvent.type(screen.getByTestId('search'), 'bbc');
      jest.runAllTimers();
      await waitFor(() => screen.getByText('No results found'));
    });
  });
  describe('when the API returns an error', () => {
    beforeEach(() => {
      nock('https://www.omdbapi.com')
        .defaultReplyHeaders({
          'access-control-allow-origin': '*'
        })
        .get(/s=bbc&apikey=.+$/).reply(400);
      render(<App />);
    });
    it('should show an error message', async () => {
      jest.spyOn(console, 'error').mockImplementation(() => { });
      jest.useFakeTimers('modern'); // for debouncing
      userEvent.type(screen.getByTestId('search'), 'bbc');
      jest.runAllTimers();
      await waitFor(() => screen.getByTestId('error-message'));
      expect(screen.getByTestId('error-message')).toHaveTextContent('Something went wrong');
    });
  });
});
