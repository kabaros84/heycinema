import { Movie } from '../../services/api';
import { renderWithContext } from '../../utils/test-utils';
import MovieCard from '../MovieCard';

describe('MovieCard', () => {
    const movie: Movie = {
        Title: 'Card',
        Year: 1990,
        imdbID: 'I-121',
        Type: 'movie',
        Poster: 'http://photo.png'
    };

    it('should render the movie card', () => {
        const providerProps = { loading: false };
        const { getByText, getByRole } = renderWithContext(<MovieCard movie={movie} />, { providerProps });

        expect(getByText(movie.Title)).toBeVisible();
        expect(getByText(movie.Year)).toBeVisible();
        expect(getByRole('img').getAttribute('src')).toEqual(movie.Poster);
    });

    it('should dim the images when loading search result', () => {
        const providerProps = { loading: true };
        const { container } = renderWithContext(<MovieCard movie={movie} />, { providerProps });
        expect(container.firstChild).toHaveStyleRule('opacity', '0.2')
    });

    it('should show default photo if no image', () => {
        const providerProps = { loading: false };
        const movieWithNoImage = { ...movie, Poster: 'N/A' }
        const { getByRole } = renderWithContext(<MovieCard movie={movieWithNoImage} />, { providerProps });

        expect(getByRole('img').getAttribute('src')).toMatch(/notApplicable.png/);
    });
});
