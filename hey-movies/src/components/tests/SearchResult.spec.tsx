import { Movie } from '../../services/api';
import { MoviesContextType } from '../../services/MoviesContext';
import { renderWithContext } from '../../utils/test-utils';
import SearchResult from '../SearchResult';

describe('SearchResult', () => {
    const movie1: Movie = {
        Title: 'God Father',
        Year: 1990,
        imdbID: 'I-121',
        Type: 'movie',
        Poster: 'http://photo.png'
    };
    const movie2: Movie = {
        Title: 'God Father 2',
        Year: 1990,
        imdbID: 'I-122',
        Type: 'movie',
        Poster: 'http://photo.png'
    };

    it('should render a grid of all the results', () => {
        const movies = [movie1, movie2];
        const searchString = 'god';
        const providerProps: Partial<MoviesContextType> = { searchResult: { searchString, movies } };
        const { queryAllByTestId } = renderWithContext(<SearchResult />, { providerProps });

        expect(queryAllByTestId('movie-card').length).toEqual(movies.length);
    });

    it('should show the seach query', () => {
        const movies = [movie1, movie2];
        const searchString = 'god';
        const providerProps: Partial<MoviesContextType> = { searchResult: { searchString, movies } };
        const { getByTestId } = renderWithContext(<SearchResult />, { providerProps });

        expect(getByTestId('search-header')).toHaveTextContent(`Search results "${searchString}"`);
    });

    it('should not show the seach header if loading', () => {
        const movies = [movie1, movie2];
        const searchString = 'god';
        const providerProps: Partial<MoviesContextType> = { loading: true, searchResult: { searchString, movies } };
        const { queryByTestId } = renderWithContext(<SearchResult />, { providerProps });

        expect(queryByTestId('search-header')).not.toBeInTheDocument();
    });

    it('should render nothing if no search result', () => {
        const providerProps: Partial<MoviesContextType> = { loading: true, searchResult: undefined };
        const { container } = renderWithContext(<SearchResult />, { providerProps });

        expect(container.firstChild).toBeNull();
    });
});
