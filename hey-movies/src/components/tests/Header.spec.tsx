import { render, screen } from '../../utils/test-utils';
import Header from '../Header';

describe('Header', () => {
    it('should show the header of the app', () => {
        render(<Header />);
        expect(screen.getByText(/hey/)).toBeVisible();
        expect(screen.getByText(/cinema/)).toBeVisible();
    });
});
