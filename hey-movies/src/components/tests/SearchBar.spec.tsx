import { MoviesContextType } from '../../services/MoviesContext';
import { renderWithContext, screen } from '../../utils/test-utils';
import SearchBar from '../SearchBar';
import userEvent from '@testing-library/user-event'

describe('SearchBar', () => {
    const setLoading = jest.fn();
    const onSearch = jest.fn();

    afterEach(() => {
        jest.clearAllMocks();
    })

    it('should show loader once user starts typing', () => {
        const providerProps: Partial<MoviesContextType> = { setLoading };
        renderWithContext(<SearchBar onSearch={onSearch} />, { providerProps });
        userEvent.type(screen.getByTestId('search'), 'a');
        expect(setLoading).toHaveBeenCalledWith(true);
    });

    it('should debounce the calls to onSearch', () => {
        jest.useFakeTimers('modern');
        const providerProps: Partial<MoviesContextType> = { setLoading };
        renderWithContext(<SearchBar onSearch={onSearch} />, { providerProps });
        expect(onSearch).not.toHaveBeenCalled();

        userEvent.type(screen.getByTestId('search'), 'forest')
        jest.advanceTimersByTime(500)
        userEvent.type(screen.getByTestId('search'), ' gump');
        jest.advanceTimersByTime(1000)
        jest.runAllTimers();
        expect(onSearch).toHaveBeenCalledTimes(1);
        expect(onSearch).toHaveBeenCalledWith('forest gump');
    });

    it('should search when Enter pressed', () => {
        jest.useFakeTimers('modern');
        const providerProps: Partial<MoviesContextType> = { setLoading };
        renderWithContext(<SearchBar onSearch={onSearch} />, { providerProps });
        expect(onSearch).not.toHaveBeenCalled();

        userEvent.type(screen.getByTestId('search'), 'forest{enter}');
        jest.runAllTimers();
        expect(onSearch).toHaveBeenCalledWith('forest');
    });

    it('should not call search if empty', () => {
        jest.useFakeTimers('modern');
        const providerProps: Partial<MoviesContextType> = { setLoading };
        renderWithContext(<SearchBar onSearch={onSearch} />, { providerProps });
        expect(onSearch).not.toHaveBeenCalled();
        const input = screen.getByTestId('search') as HTMLInputElement;
        userEvent.type(input, ' ');
        jest.runAllTimers();

        expect(onSearch).not.toHaveBeenCalled();
    });

});