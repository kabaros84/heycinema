import { render, screen } from '../../utils/test-utils';
import ErrorMessage from '../ErrorMessage';

describe('Error Message', () => {
    it('should show the error message when provided', () => {
        const message = 'something wrong';
        render(<ErrorMessage message={message} />);
        expect(screen.getByText(message)).toHaveTextContent(message);
    });
});
