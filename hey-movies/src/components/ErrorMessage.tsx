import { FunctionComponent } from "react";

export interface ErrorMessageProps {
    message: string;
}

const ErrorMessage: FunctionComponent<ErrorMessageProps> = ({ message }) => {
    return (<div data-testid="error-message">{message}</div>);
}

export default ErrorMessage;