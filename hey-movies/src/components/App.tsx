import React, { useState } from 'react';
import styled from 'styled-components';
import ErrorMessage from './ErrorMessage';
import Header from './Header';
import SearchBar from './SearchBar';
import SearchResult from './SearchResult';
import { searchApi } from '../services/api';
import { MoviesContextProvider, SearchResultType } from '../services/MoviesContext';
import { GlobalStyle } from '../styledComponents/common';

const AppContainer = styled.div`
  max-width: 1200px;
  margin: 0 auto;
`;

function App() {
  const [searchResult, setResult] = useState<SearchResultType>();
  const [loading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState<string | undefined>(undefined);

  const searchMovies = async (searchValue: string) => {
    try {
      setLoading(true);
      setResult(undefined);
      const response = await searchApi(searchValue);
      setResult({ movies: response.Search, searchString: searchValue });
      setLoading(false);
      setErrorMessage(undefined);
    } catch (err) {
      console.error(err);
      setErrorMessage(err && err.message);
    } finally {
      setLoading(false);
    }
  }

  return (
    <AppContainer>
      <GlobalStyle />
      <Header />
      <MoviesContextProvider value={{
        searchResult,
        setLoading,
        loading,
        errorMessage
      }}>
        <SearchBar onSearch={searchMovies} />
        {errorMessage && <ErrorMessage message={errorMessage} />}
        {searchResult && <SearchResult />}
      </MoviesContextProvider>
    </AppContainer>
  );
}

export default App;
