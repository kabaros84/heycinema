import { FunctionComponent } from "react";
import { useMoviesContext } from "../services/MoviesContext";
import { Heading, Subtitle } from "../styledComponents/common";
import styled, { keyframes } from "styled-components";
import MovieCard from "./MovieCard";
import { PrimaryColor, SecondaryColor } from "../config/colors";


const changeColor = keyframes`
    0% { color: ${PrimaryColor}; }
    50% { color: ${SecondaryColor}; }
    100% { color: ${PrimaryColor}; }
`;

const LoadingHeading = styled(Heading)`
    animation: ${changeColor} 1s infinite;
`
const ResultGrid = styled.div`
    display: flex;
    flex-wrap: wrap;
`;

const SearchResult: FunctionComponent = () => {

    const { searchResult, loading } = useMoviesContext();

    if (!searchResult) return null;

    const { movies = [], searchString } = searchResult;
    return (<div>
        {!loading && <Heading data-testid="search-header">Search results "{searchString}":</Heading>}
        {loading && <LoadingHeading>Searching..</LoadingHeading>}
        {!loading && movies.length === 0 && <Subtitle>No results found</Subtitle>}
        <ResultGrid>
            {movies.map((movie) => <MovieCard key={movie.imdbID} movie={movie} />)}
        </ResultGrid>
    </div>);
}

export default SearchResult;