import React, { ChangeEvent, FunctionComponent, useCallback } from 'react';
import debounce from 'lodash/debounce';
import Loader from '../styledComponents/Loader';
import { Input, InputContainer, Icon } from '../styledComponents/Input';
import { ReactComponent as SearchIcon } from '../assets/search.svg';
import { PrimaryColor } from '../config/colors';
import { useMoviesContext } from '../services/MoviesContext';
/* eslint-disable react-hooks/exhaustive-deps */

export interface SerchBarProps {
    onSearch: (search: string) => Promise<void>
}

const SerchBar: FunctionComponent<SerchBarProps> = ({ onSearch }) => {
    const { loading, setLoading } = useMoviesContext();

    const debouncedSearch = useCallback(
        debounce((value: string) => onSearch(value), 1000),
        []
    );

    const onChange = (e: ChangeEvent<HTMLInputElement>) => {
        const { value } = e.target;
        if (value && value.trim().length) {
            setLoading(true);
            debouncedSearch(value);
        }
    }

    const onKeyPress = (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter') {
            debouncedSearch(e.currentTarget.value);
        }
    }

    return (
        <InputContainer>
            <Input data-testid="search" onKeyPress={onKeyPress} onChange={onChange} placeholder="Search movies" />
            <Icon>
                {!loading && <SearchIcon color={PrimaryColor} width="16px" height="16px" />}
                {loading && <Loader small />}
            </Icon>
        </InputContainer>
    );
}

export default SerchBar;